<?php declare(strict_types=1);

/**
 * Plugin Name:     Parantion Generic Plugin
 * Version:         1.0.2
 * Description:     This module processes an order in the Parantion backend.
 * Author:          C. Balistreri (Outbox Internet), Marcel van Eijk (Parantion)
 * Author URI:      https://parantion.com/
 * Repository:      gitolite3@git.parantion.nl:webshopPlugin
 * Copyright:       (C) Outbox Internet | All rights reserved | (C) Parantion Groep BV
 * License:         Unlicensed
 */

/*
 * ACCESS
 * Get rid of users/scripts that try to call the plugin directly
 */
if(!defined('ABSPATH')) {
	exit('Whoops, it seems that you\'re not allowed to get this module directly. Maybe some other time...');
}

/*
 * ERROR HANDLING
 * Disable notices, warning and other entries, based on the debugging-mode
 */
if(defined('WP_DEBUG') && WP_DEBUG) {
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
}
else {
	error_reporting(0);
}

/*
 * DEPENDENCIES
 * Load required modules/libs
 */
require_once(__DIR__.'/lib/Config.class.php');
require_once(__DIR__.'/lib/ProcessOrder.class.php');

/*
 * CONFIGURATION
 * Let's load a configuration-set that we can use for our calls
 */
/*
if(in_array($_SERVER['REQUEST_URI'], ['/checkout/', '/cart/']) || substr($_SERVER['REQUEST_URI'], 0, 16) === '/checkout/order-') {
	$Parantion_API_Config = new Parantion_API_Config($blnParantionDebug);
}
*/

/*
 * ORDER HANDLING
 * Handle the order, make remote API calls, authenticate, create users, and much more
 */
if(substr($_SERVER['REQUEST_URI'], 0, 25) === '/checkout/order-received/' && isset($_REQUEST['key'])) {

	// Handle the order, since we're certain the order is completed and paid, so we can start our remote processes
	function processOrder($intOrderIdentifier) {
		$blnParantionUser = false;
		$blnParantionProduct = false;
		$blnParantionOrder = false;
		$blnParantionConfig = false;
		$blnParantionBackend = false;
		$objProcess = NULL;
		$strEnvironment = 'live';

		$objConfig = new Parantion_API_Config($blnParantionConfig);
		if ($objConfig->loadConfig($strEnvironment))
		{
			if ($objConfig->checkConfig())
			{
				$objProcess = new Parantion_API_ProcessOrder($objConfig, $intOrderIdentifier);
				$objProcess->setDebugUserFlag($blnParantionUser);
				$objProcess->setDebugProductFlag($blnParantionProduct);
				$objProcess->setDebugOrderFlag($blnParantionOrder);
				$objProcess->setDebugConfigFlag($blnParantionConfig);
				$objProcess->setDebugBackendFlag($blnParantionBackend);
				$objProcess->processOrder();
			}
		}

		return $objProcess;
	}

	// Implement the local method 'processOrder' into the WooCommerce hook after the payment process
	add_action('woocommerce_thankyou', 'processOrder');
}
