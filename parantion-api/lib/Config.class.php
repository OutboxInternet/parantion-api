<?php declare(strict_types=1);

if(!defined('ABSPATH')) {
	exit('Whoops, it seems that you\'re not allowed to get this module directly. Maybe some other time...');
}


/**
 * Class Parantion_API_Config
 * @package Parantion/API
 * @description Main class to handle configuration-related tasks
 * @author Carlo Balistreri <c.balistreri@outboxinternet.com>, Marcel van Eijk (Parantion)
 * @since 7.1.10
 * @version 1.0.2
 */
class Parantion_API_Config
{
	// debug
  private $blnDebugConfigFlag;
	// Properties for configuration-options
	private $strBackendEndpointV3;
	private $strBackendEndpointV42;
	private $strBackendEndpointV5;
	private $strEasionDemoCategory;
	private $arrEasionDemoAdminDatabase;
	private $arrEasionDemoAdminHash;
	private $arrEasionDemoAdminSecret;
	private $arrEasionDemoAttributeVar;
	private $arrEasionDemoAttributeType;
	private $arrEasionDemoAttributeKey;
	private $strEasionDemoUserPassword;
	private $strEasionDemoUserRole;
	private $strEasionDemoUserLanguage;
	private $strEasionDemoUserValid;
	private $strEasionDemoMailPasswordReset;
	private $strEasionDemoMailSubject;
	private $strEasionDemoMailBody;
	private $strScanJuniorLeraarCategory;
	private $strScanJuniorLeraarAdminUID;
	private $strScanJuniorLeraarAdminSID;
	private $strScanJuniorLeraarUserSID;
	private $strScanJuniorLeraarUserRole;
	private $strScanJuniorLeraarUserGroup;
	private $strFileSenderCategory;
	private $strFileSenderSuper;
	private $strFileSenderPassword;
	private $strFileSenderUser;
	public $Error, $ErrorMessage;

	// Let's get started
	public function __construct($blnDebugFlag)
	{
		$this->blnDebugConfigFlag = $blnDebugFlag;
		$this->Error = NULL;
		$this->ErrorMessage = NULL;
	}

	public function loadConfig(string $strEnvironment = ''):bool
	{
		$strConfigError = 'ParantionApi.ConfigError.';

		// Where shall we look for config-files?
		$strConfigFile = __DIR__ . '/../config/api-config';
		if ($strEnvironment != '')
		{
			$strConfigFile .= '-' . $strEnvironment;
		}
		$strConfigFile .= '.json';
		$strConfigFile = realpath($strConfigFile);

		if ($this->blnDebugConfigFlag)
		{
			echo 'debug config=file = ', $strConfigFile, "\n";
		}

		// Check for the config-file (if it exists)
		if (file_exists($strConfigFile))
		{
			$strConfigError .= 'OptionMissing.';
			$arrConfig = json_decode(file_get_contents($strConfigFile), true);

			$this->parseEndpoints($arrConfig, $strConfigError);

			$this->parseEasionDemo($arrConfig, $strConfigError);

			$this->parseScanJuniorLeraar($arrConfig, $strConfigError);

			$this->parseFileSender($arrConfig, $strConfigError);
		}
		else
		{
			// No configuration-details could be found
			$this->Error[] = $strConfigError . 'ConfigNotFound';
		}

		if ($this->blnDebugConfigFlag)
		{
			echo 'debug load-ok = ', var_export(empty($this->Error), true), "\n";
		}

		return empty($this->Error);
	}

	public function checkConfig():bool
	{
		$strConfigError = 'ParantionApi.ConfigError.';

		if ($this->blnDebugConfigFlag)
		{
			echo 'debug backend V3 = ', htmlspecialchars($this->getBackendEndpointV3()), "\n";
			echo 'debug backend V42 = ', htmlspecialchars($this->getBackendEndpointV42()), "\n";
			echo 'debug backend V5 = ', htmlspecialchars($this->getBackendEndpointV5()), "\n";
			echo 'debug easiondemo category = ', $this->getEasionDemoCategory(), "\n";
			echo 'debug easiondemo admin-database = ', var_export($this->getEasionDemoAdminDatabase(), true), "\n";
			echo 'debug easiondemo admin-hash = ', var_export($this->getEasionDemoAdminHash(), true), "\n";
			echo 'debug easiondemo admin-secret = ', var_export($this->getEasionDemoAdminSecret(), true), "\n";
			echo 'debug easiondemo attribute-var = ', var_export($this->getEasionDemoAttributeVar(), true), "\n";
			echo 'debug easiondemo attribute-type = ', var_export($this->getEasionDemoAttributeType(), true), "\n";
			echo 'debug easiondemo attribute-key = ', var_export($this->getEasionDemoAttributeKey(), true), "\n";
			echo 'debug easiondemo user-password = ', $this->getEasionDemoUserPassword(), "\n";
			echo 'debug easiondemo user-role = ', $this->getEasionDemoUserRole(), "\n";
			echo 'debug easiondemo user-language = ', $this->getEasionDemoUserLanguage(), "\n";
			echo 'debug easiondemo user-valid = ', $this->getEasionDemoUserValid(), "\n";
			echo 'debug easiondemo mail-password-reset = ', htmlspecialchars($this->getEasionDemoMailPasswordReset()), "\n";
			echo 'debug easiondemo mail-subject = ', $this->getEasionDemoMailSubject(), "\n";
			echo 'debug easiondemo mail-body = ', htmlspecialchars($this->getEasionDemoMailBody()), "\n";
			echo 'debug scan-junior-leraar category = ', $this->getScanJuniorLeraarCategory(), "\n";
			echo 'debug scan-junior-leraar admin-uid = ', $this->getScanJuniorLeraarAdminUID(), "\n";
			echo 'debug scan-junior-leraar admin-sid = ', $this->getScanJuniorLeraarAdminSID(), "\n";
			echo 'debug scan-junior-leraar user-sid = ', $this->getScanJuniorLeraarUserSID(), "\n";
			echo 'debug scan-junior-leraar user-role = ', $this->getScanJuniorLeraarUserRole(), "\n";
			echo 'debug scan-junior-leraar user-group = ', $this->getScanJuniorLeraarUserGroup(), "\n";
			echo 'debug filesender category = ', $this->getFileSenderCategory(), "\n";
			echo 'debug filesender superuser = ', $this->getFileSenderSuperUser(), "\n";
			echo 'debug filesender password = ', $this->getFileSenderPassword(), "\n";
			echo 'debug filesender user = ', $this->getFileSenderUser(), "\n";
		}

		return empty($this->Error);
	}

		// Config endpoints
	private function parseEndpoints(array $arrConfig, string $strConfigError)
	{
		$strConfigError .= 'EndPoint';

		$strOption = 'endpoint_v3';
		if (array_key_exists($strOption, $arrConfig))
		{
			$this->parseEndpointV3($arrConfig[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'endpoint_v42';
		if (array_key_exists($strOption, $arrConfig))
		{
			$this->parseEndpointV42($arrConfig[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'endpoint_v5';
		if (array_key_exists($strOption, $arrConfig))
		{
			$this->parseEndpointV5($arrConfig[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}
	}

	// Config endpoint V3
	private function parseEndpointV3(string $strEndpoint, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strEndpoint = trim($strEndpoint);

		// Match the required length, basic validation and only a safe protocol
		if(strlen($strEndpoint) > 8 && filter_var($strEndpoint, FILTER_VALIDATE_URL))
		{
			$this->strBackendEndpointV3 = (string) $strEndpoint;
		}
		else
		{
			$this->Error[] = $strConfigError . 'V3';
		}
	}

	// Config endpoint V42
	private function parseEndpointV42(string $strEndpoint, string $strConfigError)
	{

		// Clean up surrounding whitespace
		$strEndpoint = trim($strEndpoint);

		// Match the required length, basic validation and only a safe protocol
		if(strlen($strEndpoint) > 8 && filter_var($strEndpoint, FILTER_VALIDATE_URL))
		{
			$this->strBackendEndpointV42 = (string) $strEndpoint;
		}
		else
		{
			$this->Error[] = $strConfigError . 'V42';
		}
	}

	// Config endpoint V5
	private function parseEndpointV5(string $strEndpoint, string $strConfigError) {

		// Clean up surrounding whitespace
		$strEndpoint = trim($strEndpoint);

		// Match the required length, basic validation and only a safe protocol
		if(strlen($strEndpoint) > 8 && filter_var($strEndpoint, FILTER_VALIDATE_URL))
		{
			$this->strBackendEndpointV5 = (string) $strEndpoint;
		}
		else
		{
			$this->Error[] = $strConfigError . 'V5';
		}
	}

	// Config easion-demo
	private function parseEasionDemo(array $arrConfig, string $strConfigError)
	{
		$strConfigError .= 'EasionDemo.';

		$strOption = 'easiondemo';
		if (array_key_exists($strOption, $arrConfig))
		{
			$arrConfigEasionDemo = $arrConfig[$strOption];

			$strOption = 'category';
			if (array_key_exists($strOption, $arrConfigEasionDemo))
			{
				$this->parseEasionDemoCategory($arrConfigEasionDemo[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'admin';
			if (array_key_exists($strOption, $arrConfigEasionDemo))
			{
				$this->parseEasionDemoAdmin($arrConfigEasionDemo[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'attribute';
			if (array_key_exists($strOption, $arrConfigEasionDemo))
			{
				$this->parseEasionDemoAttribute($arrConfigEasionDemo[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'user';
			if (array_key_exists($strOption, $arrConfigEasionDemo))
			{
				$this->parseEasionDemoUser($arrConfigEasionDemo[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'mail';
			if (array_key_exists($strOption, $arrConfigEasionDemo))
			{
				$this->parseEasionDemoMail($arrConfigEasionDemo[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}
		}
	}

	// Config easiondemo category
	private function parseEasionDemoCategory(string $strCategory, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strCategory = trim($strCategory);

		// Let's validate the supplied hash
		if(strlen($strCategory) > 0)
		{
			$this->strEasionDemoCategory = (string) $strCategory;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Category';
		}
	}

	private function parseEasionDemoAdmin(array $arrAdminList, string $strConfigError)
	{
		$strConfigError .= 'Admin.';

		foreach ($arrAdminList as $arrAdminLine)
		{
			if (is_array($arrAdminLine))
			{
				$strOptionName = 'name';
				if (array_key_exists($strOptionName, $arrAdminLine))
				{
					$strAdminName = trim($arrAdminLine[$strOptionName]);

					$strOptionName = 'database';
					if (array_key_exists($strOptionName, $arrAdminLine))
					{
						$this->parseEasionDemoAdminDatabase($strAdminName, $arrAdminLine[$strOptionName], $strConfigError);
					}
					else
					{
						$this->Error[] = $strConfigError . 'Database';
					}

					$strOptionName = 'hash';
					if (array_key_exists($strOptionName, $arrAdminLine))
					{
						$this->parseEasionDemoAdminHash($strAdminName, $arrAdminLine[$strOptionName], $strConfigError);
					}
					else
					{
						$this->Error[] = $strConfigError . 'Hash';
					}

					$strOptionName = 'secret';
					if (array_key_exists($strOptionName, $arrAdminLine))
					{
						$this->parseEasionDemoAdminSecret($strAdminName, $arrAdminLine[$strOptionName], $strConfigError);
					}
					else
					{
						$this->Error[] = $strConfigError . 'Secret';
					}
				}
				else
				{
					$this->Error[] = $strConfigError . 'Name';
				}
			}
		}
	}

	// Config admin database
	private function parseEasionDemoAdminDatabase(string $strAdminName, string $strDatabase, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strDatabase = trim($strDatabase);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strDatabase) > 0)
		{
			$this->arrEasionDemoAdminDatabase[$strAdminName] = (string) $strDatabase;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Database';
		}
	}

	// Config admin hash
	private function parseEasionDemoAdminHash(string $strAdminName, string $strHash, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strHash = trim($strHash);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strHash) == 32)
		{
			$this->arrEasionDemoAdminHash[$strAdminName] = (string) $strHash;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Hash';
		}
	}

	// Config admin secret
	private function parseEasionDemoAdminSecret(string $strAdminName, string $strSecret, string $strConfigError)
	{
		// Let's clean up some whitespace
		$strSecret = trim($strSecret);

		// Validate the secret (only 64-chars+ allowed) and use RegEx to check the entire pattern
		if(strlen($strSecret) >= 64)
		{
			$this->arrEasionDemoAdminSecret[$strAdminName] = (string) $strSecret;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Secret';
		}
	}

	private function parseEasionDemoAttribute(array $arrAttributeList, string $strConfigError)
	{
		$strConfigError .= 'Attribute';

		foreach ($arrAttributeList as $arrAttributeLine)
		{
			if (is_array($arrAttributeLine))
			{
				$strOptionName = 'name';
				if (array_key_exists($strOptionName, $arrAttributeLine))
				{
					$strAttributeName = trim($arrAttributeLine[$strOptionName]);

					$strOptionName = 'type';
					if (array_key_exists($strOptionName, $arrAttributeLine))
					{
						$this->parseEasionDemoAttributeType($strAttributeName, $arrAttributeLine[$strOptionName], $strConfigError);
					}

					$strOptionName = 'var';
					if (array_key_exists($strOptionName, $arrAttributeLine))
					{
						$this->parseEasionDemoAttributeVar($strAttributeName, $arrAttributeLine[$strOptionName], $strConfigError);
					}
					else
					{
						$strOptionName = 'key';
						if (array_key_exists($strOptionName, $arrAttributeLine))
						{
							$this->parseEasionDemoAttributeKey($strAttributeName, $arrAttributeLine[$strOptionName], $strConfigError);
						}
						else
						{
							$this->Error[] = $strConfigError . 'Name';
						}
					}
				}
				else
				{
					$this->Error[] = $strConfigError . 'Name';
				}
			}
		}
	}

	// Config attribute type
	private function parseEasionDemoAttributeType(string $strAttributeName, string $strDatatype, string $strConfigError)
	{
		// Let's clean up some whitespace
		$strDatatype = trim($strDatatype);

		// Validate the datatype
		if(strlen($strDatatype) >= 0)
		{
			$this->arrEasionDemoAttributeType[$strAttributeName] = (string) $strDatatype;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Type.' . $strDatatype;
		}
	}

	// Config attribute var
	private function parseEasionDemoAttributeVar(string $strAttributeName, string $strVarName, string $strConfigError)
	{
		// Let's clean up some whitespace
		$strVarName = trim($strVarName);

		// Validate the var name
		if(strlen($strVarName) >= 0)
		{
			$this->arrEasionDemoAttributeVar[$strAttributeName] = (string) $strVarName;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Var.' . $strVarName;
		}
	}

	// Config attribute key
	private function parseEasionDemoAttributeKey(string $strAttributeName, string $strKeyName, string $strConfigError)
	{
		// Let's clean up some whitespace
		$strKeyName = trim($strKeyName);

		// Validate the var name
		if(strlen($strKeyName) >= 0)
		{
			$this->arrEasionDemoAttributeKey[$strAttributeName] = (string) $strKeyName;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Key.' . $strKeyName;
		}
	}

	// config user
	private function parseEasionDemoUser(array $arrUser, string $strConfigError)
	{
		$strConfigError .= 'User.';

		$strOption = 'password';
		if (array_key_exists($strOption, $arrUser))
		{
			$this->parseEasionDemoUserPassword($arrUser[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'role';
		if (array_key_exists($strOption, $arrUser))
		{
			$this->parseEasionDemoUserRole($arrUser[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'language';
		if (array_key_exists($strOption, $arrUser))
		{
			$this->parseEasionDemoUserLanguage($arrUser[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'valid';
		if (array_key_exists($strOption, $arrUser))
		{
			$this->parseEasionDemoUserValid($arrUser[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}
	}

	// Config user password
	private function parseEasionDemoUserPassword(string $strPassword, string $strConfigError) {

		// Clean up surrounding whitespace
		$strPassword = trim($strPassword);

		// Match the required length
		if(strlen($strPassword) > 8)
		{
			$this->strEasionDemoUserPassword = (string) $strPassword;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Password';
		}
	}

	// Config user role
	private function parseEasionDemoUserRole(string $strRole, string $strConfigError) {

		// Clean up surrounding whitespace
		$strRole = trim($strRole);

		// Match the required length
		if(strlen($strRole) > 8)
		{
			$this->strEasionDemoUserRole = (string) $strRole;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Role';
		}
	}

	// Config user language
	private function parseEasionDemoUserLanguage(string $strLanguage, string $strConfigError) {

		// Clean up surrounding whitespace
		$strLanguage = trim($strLanguage);

		// Match the required length
		if(strlen($strLanguage) > 8)
		{
			$this->strEasionDemoUserLanguage = (string) $strLanguage;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Language';
		}
	}

	// Config user valid
	private function parseEasionDemoUserValid(string $strValid, string $strConfigError) {

		// Clean up surrounding whitespace
		$strValid = trim($strValid);

		// Match the required length
		if(strlen($strValid) > 0)
		{
			$this->strEasionDemoUserValid = (string) $strValid;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Valid';
		}
	}

	// Config mail
	private function parseEasionDemoMail(array $arrMail, string $strConfigError)
	{
		$strConfigError .= 'Mail.';

		$strOption = 'password_reset';
		if (array_key_exists($strOption, $arrMail))
		{
			$this->parseEasionDemoMailPasswordReset($arrMail[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'subject';
		if (array_key_exists($strOption, $arrMail))
		{
			$this->parseEasionDemoMailSubject($arrMail[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'body';
		if (array_key_exists($strOption, $arrMail))
		{
			$this->parseEasionDemoMailBody($arrMail[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}
	}

	// Config mail password reset
	private function parseEasionDemoMailPasswordReset(string $strLink, string $strConfigError) {

		// Clean up surrounding whitespace
		$strLink = trim($strLink);

		// Match the required length, basic validation and only a safe protocol
		if(strlen($strLink) > 8)
		{
			$this->strEasionDemoMailPasswordReset = (string) $strLink;
		}
		else
		{
			$this->Error[] = $strConfigError . 'PasswordReset';
		}
	}

	// Config mail subject
	private function parseEasionDemoMailSubject(string $strMailSubject, string $strConfigError)
	{

		// Clean up surrounding whitespace
		$strMailSubject = trim($strMailSubject);

		// Match the required length
		if(strlen($strMailSubject) > 0)
		{
			$this->strEasionDemoMailSubject = (string) $strMailSubject;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Subject';
		}
	}

	// Config mail body
	private function parseEasionDemoMailBody(string $strMailBody, string $strConfigError)
	{

		// Clean up surrounding whitespace
		$strMailBody = trim($strMailBody);

		// Match the required length
		if(strlen($strMailBody) > 0)
		{
			$this->strEasionDemoMailBody = (string) $strMailBody;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Body';
		}
	}

	// Config scan-junior-leraar
	private function parseScanJuniorLeraar(array $arrConfig, string $strConfigError)
	{
		$strConfigError = $strConfigError . 'ScanJuniorLeraar.';

		$strOption = 'scanjuniorleraar';
		if (array_key_exists($strOption, $arrConfig))
		{
			$arrConfigScanJuniorLeraar = $arrConfig[$strOption];

			$strOption = 'category';
			if (array_key_exists($strOption, $arrConfigScanJuniorLeraar))
			{
				$this->parseScanJuniorLeraarCategory($arrConfigScanJuniorLeraar[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'admin';
			if (array_key_exists($strOption, $arrConfigScanJuniorLeraar))
			{
				$this->parseScanJuniorLeraarAdmin($arrConfigScanJuniorLeraar[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'user';
			if (array_key_exists($strOption, $arrConfigScanJuniorLeraar))
			{
				$this->parseScanJuniorLeraarUser($arrConfigScanJuniorLeraar[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}
		}
	}

	// Config scan-junior-leraar category
	private function parseScanJuniorLeraarCategory(string $strCategory, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strCategory = trim($strCategory);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strCategory) > 0)
		{
			$this->strScanJuniorLeraarCategory = (string) $strCategory;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Category';
		}
	}

	// Config scan-junior-leraar admin
	private function parseScanJuniorLeraarAdmin(array $arrAdmin, string $strConfigError)
	{
		$strConfigError .= 'Admin.';

		$strOption = 'admin_uid';
		if (array_key_exists($strOption, $arrAdmin))
		{
			$this->parseScanJuniorLeraarAdminUID($arrAdmin[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'admin_sid';
		if (array_key_exists($strOption, $arrAdmin))
		{
			$this->parseScanJuniorLeraarAdminSID($arrAdmin[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'user_sid';
		if (array_key_exists($strOption, $arrAdmin))
		{
			$this->parseScanJuniorLeraarAdminUserSID($arrAdmin[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}
	}

	// Config scan-junior-leraar admin-uid
	private function parseScanJuniorLeraarAdminUID(string $strHash, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strHash = trim($strHash);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strHash) > 0)
		{
			$this->strScanJuniorLeraarAdminUID = (string) $strHash;
		}
		else
		{
			$this->Error[] = $strConfigError . 'AdminUID';
		}
	}

	// Config scan-junior-leraar admin-sid
	private function parseScanJuniorLeraarAdminSID(string $strHash, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strHash = trim($strHash);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strHash) > 0)
		{
			$this->strScanJuniorLeraarAdminSID = (string) $strHash;
		}
		else
		{
			$this->Error[] = $strConfigError . 'AdminSID';
		}
	}

	// Config scan-junior-leraar user-sid
	private function parseScanJuniorLeraarAdminUserSID(string $strHash, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strHash = trim($strHash);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strHash) > 0)
		{
			$this->strScanJuniorLeraarUserSID = (string) $strHash;
		}
		else
		{
			$this->Error[] = $strConfigError . 'UserSID';
		}
	}

	// Config scan-junior-leraar user
	private function parseScanJuniorLeraarUser(array $arrUser, string $strConfigError)
	{
		$strConfigError .= 'User.';

		$strOption = 'role';
		if (array_key_exists($strOption, $arrUser))
		{
			$this->parseScanJuniorLeraarUserRole($arrUser[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'group';
		if (array_key_exists($strOption, $arrUser))
		{
			$this->parseScanJuniorLeraarUserGroup($arrUser[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}
	}

	// Config scan-junior-leraar user-role
	private function parseScanJuniorLeraarUserRole(string $strRole, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strRole = trim($strRole);

		// Let's validate the supplied role
		if(strlen($strRole) > 0)
		{
			$this->strScanJuniorLeraarUserRole = (string) $strRole;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Role';
		}
	}

	// Config scan-junior-leraar admin-sid
	private function parseScanJuniorLeraarUserGroup(string $strGroup, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strGroup = trim($strGroup);

		// Let's validate the supplied group
		if(strlen($strGroup) > 0)
		{
			$this->strScanJuniorLeraarUserGroup = (string) $strGroup;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Group';
		}
	}

	// Config filesender
	private function parseFileSender(array $arrConfig, string $strConfigError)
	{
		$strConfigError .= 'FileSender.';

		$strOption = 'filesender';
		if (array_key_exists($strOption, $arrConfig))
		{
			$arrConfigFileSender = $arrConfig[$strOption];

			$strOption = 'category';
			if (array_key_exists($strOption, $arrConfigFileSender))
			{
				$this->parseFileSenderCategory($arrConfigFileSender[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}

			$strOption = 'admin';
			if (array_key_exists($strOption, $arrConfigFileSender))
			{
				$this->parseFileSenderAdmin($arrConfigFileSender[$strOption], $strConfigError);
			}
			else
			{
				$this->Error[] = $strConfigError . $strOption;
			}
		}
	}

	// Config filesender category
	private function parseFileSenderCategory(string $strCategory, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strCategory = trim($strCategory);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if (strlen($strCategory) > 0)
		{
			$this->strFileSenderCategory = (string) $strCategory;
		}
		else
		{
			$this->Error[] = 'ParantionApi.ConfigError.OptionWrong.FileSender.Category';
		}
	}

	// Config filesender admin
	private function parseFileSenderAdmin(array $arrAdmin, string $strConfigError)
	{
		$strOption = 'super';
		if (array_key_exists($strOption, $arrAdmin))
		{
			$this->parseFileSenderAdminSuper($arrAdmin[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'password';
		if (array_key_exists($strOption, $arrAdmin))
		{
			$this->parseFileSenderAdminPassword($arrAdmin[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}

		$strOption = 'user';
		if (array_key_exists($strOption, $arrAdmin))
		{
			$this->parseFileSenderAdminUser($arrAdmin[$strOption], $strConfigError);
		}
		else
		{
			$this->Error[] = $strConfigError . $strOption;
		}
	}

	// Config filesender admin super
	private function parseFileSenderAdminSuper(string $strSuperName, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strSuperName = trim($strSuperName);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if (strlen($strSuperName) > 0)
		{
			$this->strFileSenderSuper = (string) $strSuperName;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Super';
		}
	}

	// Config filesender admin password
	private function parseFileSenderAdminPassword(string $strPassword, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strPassword = trim($strPassword);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strPassword) > 0)
		{
			$this->strFileSenderPassword = (string) $strPassword;
		}
		else
		{
			$this->Error[] = $strConfigError . 'Password';
		}
	}

	// Config filesender user
	private function parseFileSenderAdminUser(string $strUserName, string $strConfigError)
	{
		// Clean up surrounding whitespace
		$strUserName = trim($strUserName);

		// Let's validate the supplied hash (based in the RegEx pattern and fixed-length)
		if(strlen($strUserName) > 0)
		{
			$this->strFileSenderUser = (string) $strUserName;
		}
		else
		{
			$this->Error[] = $strConfigError . 'User';
		}
	}

	// Expose config-fields

	public function getBackendEndpointV3():string
	{
		return $this->strBackendEndpointV3;
	}

	public function getBackendEndpointV42():string
	{
		return $this->strBackendEndpointV42;
	}

	public function getBackendEndpointV5():string
	{
		return $this->strBackendEndpointV5;
	}

	public function getEasionDemoCategory():string
	{
		return $this->strEasionDemoCategory;
	}

	public function getEasionDemoAdminDatabase():array
	{
		return $this->arrEasionDemoAdminDatabase;
	}

	public function getEasionDemoAdminHash():array
	{
		return $this->arrEasionDemoAdminHash;
	}

	public function getEasionDemoAdminSecret():array
	{
		return $this->arrEasionDemoAdminSecret;
	}

	public function getEasionDemoAttributeType():array
	{
		return $this->arrEasionDemoAttributeType;
	}

	public function getEasionDemoAttributeVar():array
	{
		return $this->arrEasionDemoAttributeVar;
	}

	public function getEasionDemoAttributeKey():array
	{
		return $this->arrEasionDemoAttributeKey;
	}

	public function getEasionDemoUserPassword():string
	{
		return $this->strEasionDemoUserPassword;
	}

	public function getEasionDemoUserRole():string
	{
		return $this->strEasionDemoUserRole;
	}

	public function getEasionDemoUserLanguage():string
	{
		return $this->strEasionDemoUserLanguage;
	}

	public function getEasionDemoUserValid():string
	{
		return $this->strEasionDemoUserValid;
	}

	public function getEasionDemoMailPasswordReset():string
	{
		return $this->strEasionDemoMailPasswordReset;
	}

	public function getEasionDemoMailSubject():string
	{
		return $this->strEasionDemoMailSubject;
	}

	public function getEasionDemoMailBody():string
	{
		return $this->strEasionDemoMailBody;
	}

	public function getScanJuniorLeraarCategory():string
	{
		return $this->strScanJuniorLeraarCategory;
	}

	public function getScanJuniorLeraarAdminUID():string
	{
		return $this->strScanJuniorLeraarAdminUID;
	}

	public function getScanJuniorLeraarAdminSID():string
	{
		return $this->strScanJuniorLeraarAdminSID;
	}

	public function getScanJuniorLeraarUserSID():string
	{
		return $this->strScanJuniorLeraarUserSID;
	}

	public function getScanJuniorLeraarUserRole():string
	{
		return $this->strScanJuniorLeraarUserRole;
	}

	public function getScanJuniorLeraarUserGroup():string
	{
		return $this->strScanJuniorLeraarUserGroup;
	}

	public function getFileSenderCategory():string
	{
		return $this->strFileSenderCategory;
	}

	public function getFileSenderSuperUser():string
	{
		return $this->strFileSenderSuper;
	}

	public function getFileSenderPassword():string
	{
		return $this->strFileSenderPassword;
	}

	public function getFileSenderUser():string
	{
		return $this->strFileSenderUser;
	}
}
