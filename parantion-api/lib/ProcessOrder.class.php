<?php declare(strict_types=1);

if(!defined('ABSPATH')) {
	exit('Whoops, it seems that you\'re not allowed to get this module directly. Maybe some other time...');
}

require_once(__DIR__.'/BackendEasionDemo.class.php');
require_once(__DIR__.'/BackendScanJuniorLeraar.class.php');

/**
 * Class Parantion_API_ProcessOrder
 * @package Parantion/API
 * @description Class that handles order-related tasks
 * @author Carlo Balistreri <c.balistreri@outboxinternet.com>, Marcel van Eijk (Parantion)
 * @since 7.1.10
 * @version 1.0.2
 */
class Parantion_API_ProcessOrder {
	// debug
	private $blnDebugUserFlag;
	private $blnDebugProductFlag;
	private $blnDebugOrderFlag;
	private $blnDebugConfigFlag;
	private $blnDebugBackendFlag;
	// config
	/**
	 * @var Parantion_API_Config $objConfig
	 */
	private $objConfig;
	// order-details
	private $intOrderID;
	/**
	 * @var WC_Order $objOrder
	 */
	private $objOrder;
	// error message
  private $strErrorBody;
  // item list
	private $arrItemList;
	// user hash
	private $strUserHash;

	// Let's get started
	public function __construct($objConfig, $intOrderIdentifier)
	{
		$this->objConfig = $objConfig;
		$this->intOrderID = $intOrderIdentifier;
		$this->blnDebugUserFlag = false;
		$this->blnDebugProductFlag = false;
		$this->blnDebugOrderFlag = false;
		$this->blnDebugConfigFlag = false;
		$this->blnDebugBackendFlag = false;
	}

	public function setDebugUserFlag($blnUserInfoFlag)
	{
		$this->blnDebugUserFlag = $blnUserInfoFlag;
	}

	public function setDebugProductFlag($blnProductFlag)
	{
		$this->blnDebugProductFlag = $blnProductFlag;
	}

	public function setDebugOrderFlag($blnOrderFlag)
	{
		$this->blnDebugOrderFlag = $blnOrderFlag;
	}

	public function setDebugConfigFlag($blnConfigFlag)
	{
		$this->blnDebugConfigFlag = $blnConfigFlag;
	}

	public function setDebugBackendFlag($blnBackendFlag)
	{
		$this->blnDebugBackendFlag = $blnBackendFlag;
	}

	public function processOrder()
	{
		$blnOkFlag = true;
		$this->strErrorBody = '';

		if ($this->blnDebugOrderFlag)
		{
			echo 'debug start process order ', $this->intOrderID, "\n";
		}

		// check if order can be done
		if (!is_numeric($this->intOrderID))
		{
			if ($this->blnDebugOrderFlag)
			{
				echo 'debug order invalid', "\n";
			}

			$blnOkFlag = false;
			$this->strErrorBody = 'Order identifier invalid: ' . $this->intOrderID;
		}
		elseif (!($this->objConfig instanceof Parantion_API_Config))
		{
			if ($this->blnDebugConfigFlag)
			{
				echo 'debug config not available', "\n";
			}
			$blnOkFlag = false;
			$this->strErrorBody = 'Parantion config is not available';
		}

		// get order details
		if ($blnOkFlag)
		{
			if ($this->blnDebugOrderFlag)
			{
				echo 'debug get order', "\n";
			}
			$blnOkFlag = $this->getOrder();
		}

		// get product attributes
		if ($blnOkFlag)
		{
			if ($this->blnDebugOrderFlag)
			{
				echo 'debug check items', "\n";
			}
			$blnOkFlag = $this->checkItems();
		}

		if ($blnOkFlag)
		{
			if ($this->blnDebugOrderFlag)
			{
				echo 'debug process items', "\n";
			}
			$blnOkFlag = $this->processItems();
		}

		if ($blnOkFlag)
		{
			if ($this->blnDebugOrderFlag)
			{
				echo 'debug finalize order', "\n";
			}
			$this->finalizeOrder();
		}

		if ($this->blnDebugOrderFlag)
		{
			if ($blnOkFlag)
			{
				wp_die('process order done');
			}
			else
			{
				wp_die('process order error: ' . $this->strErrorBody);
			}
		}

		if (!$blnOkFlag)
		{
			// If anything goes wrong, let the site-admin know. Demo email is included.
			wp_mail(
				get_option('admin_email'),
				'WooCommerce Parantion plugin, process order error',
				$this->strErrorBody);
		}
	}

	private function getOrder()
	{
		$blnOkFlag = true;

		$this->objOrder = WC_Order_Factory::get_order($this->intOrderID);

		if ($this->objOrder instanceof WC_Order)
		{
			if (!$this->blnDebugOrderFlag)
			{
				// Only proceed when the order isn't completed (so we don't create duplicate accounts)
				if ($this->objOrder->get_status() === 'completed')
				{
					$blnOkFlag = false;
					$this->strErrorBody = 'Order already completed: ' . $this->intOrderID;
				}
			}
		}
		else
		{
			$blnOkFlag = false;
			$this->strErrorBody = 'Order not found: ' . $this->intOrderID;
		}

		return $blnOkFlag;
	}

	private function checkItems()
	{
		$blnOkFlag = true;

		$this->arrItemList = NULL;

		$arrItems = $this->objOrder->get_items();

		if ($this->blnDebugOrderFlag)
		{
			echo 'debug check items', "\n";
		}

		foreach($arrItems as $objItem)
		{
			// assume no valid item
			$arrItemInfo = NULL;

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug check item class', "\n";
			}

			if ($objItem instanceof WC_Order_Item_Product)
			{
				$intProductID = $objItem->get_product_id();

				if ($this->blnDebugProductFlag)
				{
					echo 'debug check product ', $intProductID, "\n";
				}

				if ($intProductID > 0)
				{
					// get product-category
					$objProduct = new WC_Product($intProductID);

					if ($this->blnDebugProductFlag)
					{
						echo 'debug check product class', "\n";
					}

					if ($objProduct instanceof WC_Product)
					{
						$arrItemInfo['product'] = $objProduct->get_slug();

						// $arrCategoryList = explode(wc_get_product_category_list($intProductID, ';'), ';');
						$arrCategoryList = $objProduct->get_category_ids();

						if ($this->blnDebugProductFlag)
						{
							echo 'debug category-list ', var_export($arrCategoryList, true), "\n";
						}

						// get first category and convert to slug
						$intCategoryID = reset($arrCategoryList);
						$objTerm = get_term_by('id', $intCategoryID, 'product_cat');
						$arrItemInfo['category'] = $objTerm->slug;

						if ($this->blnDebugProductFlag)
						{
							echo 'debug category-slug ', $arrItemInfo['category'], "\n";
						}

						$arrAttributes = $objProduct->get_attributes();
						if (is_array($arrAttributes))
						{
							if ($this->blnDebugProductFlag)
							{
								echo 'debug product attributes ', var_export($arrAttributes, true), "\n";
							}
							$arrAttributeList = NULL;
							foreach ($arrAttributes as $strAttributeName => $objAttributeValue)
							{
								$arrOptions = $objAttributeValue->get_options();
								$strOptionValue = reset($arrOptions);

								if ($this->blnDebugProductFlag)
								{
									echo 'debug option ', $strAttributeName, '=', $strOptionValue, "\n";
								}

								$arrAttributeList[$strAttributeName] = $strOptionValue;
							}
							$arrItemInfo['attribute'] = $arrAttributeList;
						}
					}
					else
					{
						$blnOkFlag = false;
						$this->strErrorBody = 'No product found: ' . $intProductID;
					}
				}
				else
				{
					$blnOkFlag = false;
					$this->strErrorBody = 'No product in order: ' . $this->intOrderID;
				}
			}
			else
			{
				$blnOkFlag = false;
				$this->strErrorBody = 'Order item is not a product: ' . $this->intOrderID;
			}

			if (is_array($arrItemInfo))
			{
				$this->arrItemList[] = $arrItemInfo;
			}
		}

		return $blnOkFlag;
	}

	private function processItems()
	{
		$blnOkFlag = true;

		// get user info from order
		$strUserFirstName = $this->objOrder->get_billing_first_name();
		$strUserLastName = $this->parseLastName($this->objOrder->get_billing_last_name());
		$strUserMiddleName = $this->parseMiddleName($this->objOrder->get_billing_last_name());
		$strUserEmail = $this->objOrder->get_billing_email();

		foreach ($this->arrItemList as $arrItemInfo)
		{
			if ($this->blnDebugOrderFlag)
			{
				echo 'debug process items ', var_export($arrItemInfo, true), "\n";
			}

			$strCategory = NULL;
			if (array_key_exists('category', $arrItemInfo))
			{
				$strCategory = $arrItemInfo['category'];
			}

			switch ($strCategory)
			{
				case 'easiondemo':
					// process easion-demo product
					$objBackendEasionDemo = new Parantion_API_BackendEasionDemo();
					$objBackendEasionDemo->setDebugUserFlag($this->blnDebugUserFlag);
					$objBackendEasionDemo->setDebugProductFlag($this->blnDebugProductFlag);
					$objBackendEasionDemo->setDebugOrderFlag($this->blnDebugOrderFlag);
					$objBackendEasionDemo->setDebugConfigFlag($this->blnDebugConfigFlag);
					$objBackendEasionDemo->setDebugBackendFlag($this->blnDebugBackendFlag);
					$objBackendEasionDemo->setBackendV42Link($this->objConfig->getBackendEndpointV42());
					$objBackendEasionDemo->setBackendV5Link($this->objConfig->getBackendEndpointV5());
					$objBackendEasionDemo->setAdminDatabase($this->objConfig->getEasionDemoAdminDatabase());
					$objBackendEasionDemo->setAdminHash($this->objConfig->getEasionDemoAdminHash());
					$objBackendEasionDemo->setAdminSecret($this->objConfig->getEasionDemoAdminSecret());
					$objBackendEasionDemo->setMailSubject($this->objConfig->getEasionDemoMailSubject());
					$objBackendEasionDemo->setMailBody($this->objConfig->getEasionDemoMailBody());
					$objBackendEasionDemo->setLinkPasswordReset($this->objConfig->getEasionDemoMailPasswordReset());
					$objBackendEasionDemo->setUserPassword($this->objConfig->getEasionDemoUserPassword());
					$objBackendEasionDemo->setUserRole($this->objConfig->getEasionDemoUserRole());
					$objBackendEasionDemo->setUserLanguage($this->objConfig->getEasionDemoUserLanguage());
					$objBackendEasionDemo->setUserValid($this->objConfig->getEasionDemoUserValid());
					$objBackendEasionDemo->setUserFirstName($strUserFirstName);
					$objBackendEasionDemo->setUserMiddleName($strUserMiddleName);
					$objBackendEasionDemo->setUserLastName($strUserLastName);
					$objBackendEasionDemo->setUserEmail($strUserEmail);
					$objBackendEasionDemo->setAttributeVar($this->objConfig->getEasionDemoAttributeVar());
					$objBackendEasionDemo->setAttributeType($this->objConfig->getEasionDemoAttributeType());
					$objBackendEasionDemo->setAttributeKey($this->objConfig->getEasionDemoAttributeKey());
					$arrAttributeProduct = NULL;
					if (array_key_exists('attribute', $arrItemInfo))
					{
						$objBackendEasionDemo->setAttributeProduct($arrItemInfo['attribute']);
					}

					$blnOkFlag = $objBackendEasionDemo->processOrder();
					break;
				case 'scan-junior-leraar':
					// process scan-junior-leraar product
					$objBackendScanJuniorLeraar = new Parantion_API_BackendScanJuniorLeraar();
					$objBackendScanJuniorLeraar->setDebugUserFlag($this->blnDebugUserFlag);
					$objBackendScanJuniorLeraar->setDebugProductFlag($this->blnDebugProductFlag);
					$objBackendScanJuniorLeraar->setDebugOrderFlag($this->blnDebugOrderFlag);
					$objBackendScanJuniorLeraar->setDebugConfigFlag($this->blnDebugConfigFlag);
					$objBackendScanJuniorLeraar->setDebugBackendFlag($this->blnDebugBackendFlag);
					$objBackendScanJuniorLeraar->setBackendV3Link($this->objConfig->getBackendEndpointV3());
					$objBackendScanJuniorLeraar->setAdminUID($this->objConfig->getScanJuniorLeraarAdminUID());
					$objBackendScanJuniorLeraar->setAdminSID($this->objConfig->getScanJuniorLeraarAdminSID());
					$objBackendScanJuniorLeraar->setUserSID($this->objConfig->getScanJuniorLeraarUserSID());
					$objBackendScanJuniorLeraar->setUserRole($this->objConfig->getScanJuniorLeraarUserRole());
					$objBackendScanJuniorLeraar->setUserGroup($this->objConfig->getScanJuniorLeraarUserGroup());
					$objBackendScanJuniorLeraar->setUserFirstName($strUserFirstName);
					$objBackendScanJuniorLeraar->setUserMiddleName($strUserMiddleName);
					$objBackendScanJuniorLeraar->setUserLastName($strUserLastName);
					$objBackendScanJuniorLeraar->setUserEmail($strUserEmail);

					$objBackendScanJuniorLeraar->processOrder();
					break;
				case 'credits':
					break;
			}
		}

		return $blnOkFlag;
	}

	// Mark the order as 'complete' and save the user-hash as a note and user-metadata
	private function finalizeOrder()
	{
		$strStatusText = 'done';
		if (!empty($this->strUserHash))
		{
			$strStatusText = 'hash=' . $this->strUserHash . ';';
		}
		// Mark the order as 'complete'
		$this->objOrder->update_status('completed', $strStatusText);

		// Store the user-text as meta-data, connected to the user (if they're not checking out as a 'guest')
		if ((!empty($this->strUserHash)) && ($this->objOrder->get_customer_id() !== 0))
		{
			add_user_meta($this->objOrder->get_customer_id(), 'parantion_user_hash', $this->strUserHash, true);
		}
	}

	private function parseLastName(string $strName)
	{
		$intUpperPos = $this->findUpperCase($strName);

		$strLastName = '';
		if ($intUpperPos >= 0)
		{
			$strLastName = trim(substr($strName, $intUpperPos));
		}

		return $strLastName;
	}

	private function parseMiddleName(string $strName)
	{
		$intUpperPos = $this->findUpperCase($strName);

		$strMiddleName = NULL;
		if ($intUpperPos > 0)
		{
			$strMiddleName = trim(substr($strName, 0, $intUpperPos));
		}
		return $strMiddleName;
	}

	private function findUpperCase(string $strName)
	{
		$intUpperPos = -1;

		$intIndex = 0;
		while ($intUpperPos < 0)
		{
			$strToken = $strName[$intIndex];

			if (($strToken >= 'A') && ($strToken <= 'Z'))
			{
				$intUpperPos = $intIndex;
			}
			else
			{
				$intIndex++;
			}
		}

		return $intUpperPos;
	}
}