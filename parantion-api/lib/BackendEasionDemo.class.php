<?php

class Parantion_API_BackendEasionDemo
{
	private $blnDebugUserFlag;
	private $blnDebugProductFlag;
	private $blnDebugOrderFlag;
	private $blnDebugConfigFlag;
	private $blnDebugBackendFlag;
	// config
	private $strBackendV42Link;
	private $strBackendV5Link;
	// admin info
	private $arrAdminDatabase;
	private $arrAdminHash;
	private $arrAdminSecret;
	// mail info
	private $strMailSubject;
	private $strMailBody;
	private $strLinkPasswordReset;
	// backend login
	private $strDatabase;
	private $strAdminHash;
	private $strAdminSecret;
	// backend access
	private $strBackendToken;
	// user info
	private $strUserFirstName;
	private $strUserMiddleName;
	private $strUserLastName;
	private $strUserEmail;
	private $strUserPassword;
	private $strUserRole;
	private $strUserLanguage;
	private $strUserValid;
	private $strUserHash;
	// product info
	private $arrAttributeVar;
	private $arrAttributeType;
	private $arrAttributeKey;
  private $arrProductField;

	public function __construct()
	{
		$this->blnDebugUserFlag = false;
		$this->blnDebugProductFlag = false;
		$this->blnDebugOrderFlag = false;
		$this->blnDebugConfigFlag = false;
		$this->blnDebugBackendFlag = false;
		$this->strBackendV42Link = NULL;
		$this->strBackendV5Link = NULL;
		$this->arrAdminDatabase = NULL;
		$this->arrAdminHash = NULL;
		$this->arrAdminSecret = NULL;
		$this->strMailSubject = NULL;
		$this->strMailBody = NULL;
		$this->strLinkPasswordReset = NULL;
		$this->strDatabase = NULL;
		$this->strAdminHash = NULL;
		$this->strAdminSecret = NULL;
		$this->strBackendToken = NULL;
		$this->strUserFirstName = NULL;
		$this->strUserMiddleName = NULL;
		$this->strUserLastName = NULL;
		$this->strUserEmail = NULL;
		$this->strUserPassword = NULL;
		$this->strUserRole = NULL;
		$this->strUserLanguage = NULL;
		$this->strUserValid = NULL;
		$this->strUserHash = NULL;
		$this->arrAttributeVar = NULL;
		$this->arrAttributeType = NULL;
		$this->arrAttributeKey = NULL;
		$this->arrProductField = NULL;
	}

	// debug function

	public function setDebugUserFlag($blnDebugFlag)
	{
		$this->blnDebugUserFlag = $blnDebugFlag;
	}

	public function setDebugProductFlag($blnDebugFlag)
	{
		$this->blnDebugProductFlag = $blnDebugFlag;
	}

	public function setDebugOrderFlag($blnDebugFlag)
	{
		$this->blnDebugOrderFlag = $blnDebugFlag;
	}

	public function setDebugConfigFlag($blnDebugFlag)
	{
		$this->blnDebugConfigFlag = $blnDebugFlag;
	}

	public function setDebugBackendFlag($blnDebugFlag)
	{
		$this->blnDebugBackendFlag = $blnDebugFlag;
	}

	// backend function

	public function setBackendV42Link($strBackendLink)
	{
		$this->strBackendV42Link = $strBackendLink;
	}

	public function setBackendV5Link($strBackendLink)
	{
		$this->strBackendV5Link = $strBackendLink;
	}

	// admin function

	public function setAdminDatabase($arrAdminDatabase)
	{
		$this->arrAdminDatabase = $arrAdminDatabase;
	}

	public function setAdminHash($arrAdminHash)
	{
		$this->arrAdminHash = $arrAdminHash;
	}

	public function setAdminSecret($arrAdminSecret)
	{
		$this->arrAdminSecret = $arrAdminSecret;
	}

	// mail function

	public function setMailSubject($strMailSubject)
	{
		$this->strMailSubject = $strMailSubject;
	}

	public function setMailBody($strMailBody)
	{
		$this->strMailBody = $strMailBody;
	}

	public function setLinkPasswordReset($strLinkPasswordReset)
	{
		$this->strLinkPasswordReset = $strLinkPasswordReset;
	}

	// user config function

	public function setUserPassword($strPassword)
	{
		$this->strUserPassword = $strPassword;
	}

	public function setUserRole($strRole)
	{
		$this->strUserRole = $strRole;
	}

	public function setUserLanguage($strLanguage)
	{
		$this->strUserLanguage = $strLanguage;
	}

	public function setUserValid($strValid)
	{
		$this->strUserValid = $strValid;
	}

	// user function

	public function setUserFirstName($strFirstName)
	{
		$this->strUserFirstName = $strFirstName;
	}

	public function setUserMiddleName($strMiddleName)
	{
		$this->strUserMiddleName = $strMiddleName;
	}

	public function setUserLastName($strLastName)
	{
		$this->strUserLastName = $strLastName;
	}

	public function setUserEmail($strEmail)
	{
		$this->strUserEmail = $strEmail;
	}

	public function getUserHash()
	{
		return $this->strUserHash;
	}

	// product function

	public function setAttributeVar($arrAttributeVar)
	{
		$this->arrAttributeVar = $arrAttributeVar;
	}

	public function setAttributeType($arrAttributeType)
	{
		$this->arrAttributeType = $arrAttributeType;
	}

	public function setAttributeKey($arrAttributeKey)
	{
		$this->arrAttributeKey = $arrAttributeKey;
	}

	public function setAttributeProduct($arrAttributeProduct)
	{
		if ($this->blnDebugConfigFlag)
		{
			echo 'debug attribute product ', var_export($arrAttributeProduct, true), "\n";
			echo 'debug attribute var ', var_export($this->arrAttributeVar, true), "\n";
			echo 'debug attribute key ', var_export($this->arrAttributeKey, true), "\n";
			echo 'debug admin database ', var_export($this->arrAdminDatabase, true), "\n";
			echo 'debug admin hash ', var_export($this->arrAdminHash, true), "\n";
			echo 'debug admin secret ', var_export($this->arrAdminSecret, true), "\n";
		}

		foreach($this->arrAttributeVar as $strAttributeName => $strAttributeVar)
		{
			if (array_key_exists($strAttributeName, $arrAttributeProduct))
			{
				$strDatatype = 'string';
				if (array_key_exists($strAttributeName, $this->arrAttributeType))
				{
					$strDatatype = $this->arrAttributeType[$strAttributeName];
				}
				$varAttributeValue = $arrAttributeProduct[$strAttributeName];
				switch ($strDatatype)
				{
					case 'int':
						$varAttributeValue = intval($varAttributeValue);
						break;
				}
				$this->arrProductField[$strAttributeVar] = $varAttributeValue;
			}
		}

		if ($this->blnDebugConfigFlag)
		{
			echo 'debug attribute product var ', var_export($this->arrProductField, true), "\n";
		}

		foreach($this->arrAttributeKey as $strAttributeName => $strAttributeKey)
		{
			switch ($strAttributeName)
			{
				case 'database':
					if (array_key_exists($strAttributeName, $arrAttributeProduct))
					{
						$strAdminName = $arrAttributeProduct[$strAttributeName];

						if (is_array($this->arrAdminDatabase))
						{
							if (array_key_exists($strAdminName, $this->arrAdminDatabase))
							{
								$this->strDatabase = $this->arrAdminDatabase[$strAdminName];
							}
						}
						if (is_array($this->arrAdminHash))
						{
							if (array_key_exists($strAdminName, $this->arrAdminHash))
							{
								$this->strAdminHash = $this->arrAdminHash[$strAdminName];
							}
						}
						if (is_array($this->arrAdminSecret))
						{
							if (array_key_exists($strAdminName, $this->arrAdminSecret))
							{
								$this->strAdminSecret = $this->arrAdminSecret[$strAdminName];
							}
						}
					}
					break;
			}
		}

		if ($this->blnDebugConfigFlag)
		{
			echo 'debug attribute product key database ', $this->strDatabase, "\n";
			echo 'debug attribute product key admin-hash ', $this->strAdminHash, "\n";
			echo 'debug attribute product key admin-secret ', $this->strAdminSecret, "\n";
		}
	}

	public function processOrder():bool
	{
		$blnOkFlag = $this->loginBackend();

		if ($this->blnDebugOrderFlag)
		{
			echo 'debug login backend ', var_export($blnOkFlag, true), "\n";
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->createDemoSubject();

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug create demo subjects ', var_export($blnOkFlag, true), "\n";
			}

		}
		if ($blnOkFlag)
		{
			$blnOkFlag = $this->sendPasswordMail();

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug send password mail ', var_export($blnOkFlag, true), "\n";
			}
		}

		$this->logoutBackend();

		return $blnOkFlag;
	}

	// check config valid

	private function checkConfig(bool $blnLoginFlag)
	{
		$blnCheckFlag = true;

		// check backend-link
		if (empty($this->strBackendV42Link) ||
				empty($this->strBackendV5Link))
		{
			$blnCheckFlag = false;

			if ($this->blnDebugConfigFlag)
			{
				echo 'debug check config, no backend link', "\n";
			}
		}

		// check admin
		if (empty($this->strDatabase) ||
				empty($this->strAdminHash) ||
				empty($this->strAdminSecret))
		{
			$blnCheckFlag = false;

			if ($this->blnDebugConfigFlag)
			{
				echo 'debug check config, no admin login', "\n";
			}
		}

		// check mail
		if (empty($this->strMailSubject) ||
				empty($this->strMailBody))
		{
			$blnCheckFlag = false;

			if ($this->blnDebugConfigFlag)
			{
				echo 'debug check config, no mail-setup', "\n";
			}
		}

		if ($blnLoginFlag)
		{
			// check backend-token
			if (empty($this->strBackendToken))
			{
				$blnCheckFlag = false;

				if ($this->blnDebugConfigFlag)
				{
					echo 'debug check config, no backend token', "\n";
				}
			}
		}

		return $blnCheckFlag;
	}

	// login to backend en get backend-token
	public function loginBackend():bool
	{
		$blnOkFlag = $this->checkConfig(false);

		if ($blnOkFlag)
		{
			$strBackendLink = $this->strBackendV42Link . '/accounts/grant';
			$arrLoginBody = array(
				'hash' => $this->strAdminHash,
				'secret' => $this->strAdminSecret);
			$arrLoginHeader = array(
				'cache-control: no-cache',
				'content-type: application/x-www-form-urlencoded',
				'X-Database: ' . $this->strDatabase);

			if ($this->blnDebugBackendFlag)
			{
				echo "\n";
				echo 'debug login-link = ', $strBackendLink, "\n";
				echo 'debug login headers = ', var_export($arrLoginHeader, true), "\n";
				echo 'debug login body = ', var_export($arrLoginBody, true), "\n";
			}

			// backend is REST-interface, V2 POST accounts/grant
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($hndCall, CURLOPT_HTTPHEADER, $arrLoginHeader);
			curl_setopt($hndCall, CURLOPT_POSTFIELDS, http_build_query($arrLoginBody));
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug login response = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response =', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Only proceed when the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				// Turn the response in an array
				$arrLoginResponse = json_decode($varResponse, true);

				// get backend-token from response
				$strTokenField = 'token';
				if (array_key_exists($strTokenField, $arrLoginResponse))
				{
					$this->strBackendToken = $arrLoginResponse[$strTokenField];
				}
				else
				{
					$blnOkFlag = false;

					if ($this->blnDebugBackendFlag)
					{
						echo 'debug no token in login-response ', var_export($arrLoginResponse, true), "\n";
					}
				}
			}
			else
			{
				$blnOkFlag = false;
			}

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug login token = ', $this->strBackendToken, "\n";
			}
		}

		return $blnOkFlag;
	}

	// logout backend with backend-token
	public function logoutBackend()
	{
		$blnOkFlag = $this->checkConfig(true);

		if ($blnOkFlag)
		{
			$strBackendLink = $this->strBackendV42Link . '/accounts/grant';
			$arrLogoutHeader = array(
				'cache-control: no-cache',
				'X-CSRFToken: ' . $this->strBackendToken,
				'X-Database: ' . $this->strDatabase);

			if ($this->blnDebugBackendFlag)
			{
				echo "\n";
				echo 'debug logout-link = ', $strBackendLink, "\n";
				echo 'debug logout headers =', var_export($arrLogoutHeader, true), "\n";
			}

			// backend is REST-interface, V2 DELETE accounts/grant
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($hndCall, CURLOPT_HTTPHEADER, $arrLogoutHeader);
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug logout backend = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response = ', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Only proceed when the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				$blnOkFlag = true;
			}
			else
			{
				$blnOkFlag = false;
			}
		}

		return $blnOkFlag;
	}

	// create new demo-subject with product-fields
	public function createDemoSubject(): bool
	{
		$blnOkFlag = $this->checkConfig(true);

		if ($blnOkFlag)
		{
			$strBackendLink = $this->strBackendV5Link . '/user';
			$arrCreateHeader = array(
				'cache-control: no-cache',
				'content-type: application/x-www-form-urlencoded',
				'X-CSRFToken: ' . $this->strBackendToken,
				'X-Database: ' . $this->strDatabase
			);

			// build subject-fields
			if (! empty($this->strUserFirstName))
			{
				$arrUserFields['firstname'] = $this->strUserFirstName;
			}
			if (!empty($this->strUserMiddleName))
			{
				$arrUserFields['middlename'] = $this->strUserMiddleName;
			}
			$arrUserFields['sub_language'] = $this->strUserLanguage;

			if (is_array($this->arrProductField))
			{
				foreach ($this->arrProductField as $strFieldName => $varFieldValue)
				{
					$arrUserFields[$strFieldName] = $varFieldValue;
				}
			}
			$objAccountValid = new DateTime('now');
			try
			{
				$objValidOneMonth = new DateInterval('P' . $this->strUserValid);
				$objAccountValid->add($objValidOneMonth);
			} catch (Exception $e)
			{
				if ($this->blnDebugOrderFlag)
				{
					echo 'debug account-valid is not set', "\n";
				}
			}
			$arrUserFields['account_valid'] = $objAccountValid->format('Y-m-d\TH:i:s');

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug create-link = ', $strBackendLink, "\n";
				echo 'debug create-header = ', var_export($arrCreateHeader, true), "\n";
			}

			$arrUserRoles = explode(',', $this->strUserRole);
			$arrCreateBody = array(
				'username' => (string)$this->strUserEmail,
				'password' => (string)$this->strUserPassword,
				'email' => (string)$this->strUserEmail,
				'status' => (int)1,
				'role' => $arrUserRoles,
				'agreement' => (int)1,
				'lastname' => (string)$this->strUserLastName,
				'field' => $arrUserFields
			);

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug email = ', $this->strUserEmail, "\n";
				echo 'debug lastname = ', $this->strUserLastName, "\n";
				echo 'debug password = ', $this->strUserPassword, "\n";
				echo 'debug user-roles = ', var_export($arrUserRoles, true), "\n";
				echo 'debug user-fields = ', var_export($arrUserFields, true), "\n";
			}

			// backend is REST-interface, V5 POST user
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($hndCall, CURLOPT_HTTPHEADER, $arrCreateHeader);
			curl_setopt($hndCall, CURLOPT_POSTFIELDS, http_build_query($arrCreateBody));
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug create subject = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response = ', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Proceed when we're certain that the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				// Turn the response into a new object
				$arrCreateResponse = json_decode($varResponse, true);

				// get user hash from response
				$strHashField = 'uh';
				if (array_key_exists($strHashField, $arrCreateResponse))
				{
					$this->strUserHash = (string)$arrCreateResponse[$strHashField];
				}
				else
				{
					$blnOkFlag = false;
				}
			}
			else
			{
				$blnOkFlag = false;
			}
		}

		return $blnOkFlag;
	}

	// Let's make the call: send a password mail to the new user
	public function sendPasswordMail():bool
	{
		$blnOkFlag = $this->checkConfig(true);

		if ($blnOkFlag)
		{
			$strMailPasswordReset = str_replace('{database}', $this->strDatabase, $this->strLinkPasswordReset);
			$strBackendLink = $this->strBackendV42Link . '/users/' . $this->strUserHash . '/password/reset';
			$arrMailHeader = array(
				'cache-control: no-cache',
				'content-type: application/x-www-form-urlencoded',
				'X-CSRFToken: ' . $this->strBackendToken,
				'X-Database: ' . $this->strDatabase);
			$arrPasswordBody = array(
				'subject' => $this->strMailSubject,
				'body' => $this->strMailBody,
				'new' => '1',
				'link' => $strMailPasswordReset);

			if ($this->blnDebugBackendFlag)
			{
				echo "\n";
				echo 'debug mail-link = ', $strBackendLink, "\n";
				echo 'debug mail-header = ', var_export($arrMailHeader, true), "\n";
				echo 'debug password-fields = ', var_export($arrPasswordBody, true), "\n";
			}

			// Create another cURL POST call and handle the response
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($hndCall, CURLOPT_HTTPHEADER, $arrMailHeader);
			curl_setopt($hndCall, CURLOPT_POSTFIELDS, http_build_query($arrPasswordBody));
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug send password = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response = ', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Proceed when we're certain that the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				$blnOkFlag = true;
			}
			else
			{
				$blnOkFlag = false;
			}
		}

		return $blnOkFlag;
	}
}