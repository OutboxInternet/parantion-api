<?php

class Parantion_API_BackendScanJuniorLeraar
{
	private $blnDebugUserFlag;
	private $blnDebugProductFlag;
	private $blnDebugOrderFlag;
	private $blnDebugConfigFlag;
	private $blnDebugBackendFlag;
	// config
	private $strBackendV3Link;
	// backend login
	private $strAdminUID;
	private $strAdminSID;
	private $strUserSID;
	// user info
	private $strUserFirstName;
	private $strUserMiddleName;
	private $strUserLastName;
	private $strUserEmail;
	private $strUserName;
	private $strUserRole;
	private $strUserGroup;
	private $strUserHash;

	public function __construct()
	{
		$this->blnDebugUserFlag = false;
		$this->blnDebugProductFlag = false;
		$this->blnDebugOrderFlag = false;
		$this->blnDebugConfigFlag = false;
		$this->blnDebugBackendFlag = false;
		$this->strBackendV3Link = NULL;
		$this->strAdminUID = NULL;
		$this->strAdminSID = NULL;
		$this->strUserSID = NULL;
		$this->strUserFirstName = NULL;
		$this->strUserMiddleName = NULL;
		$this->strUserLastName = NULL;
		$this->strUserEmail = NULL;
		$this->strUserRole = NULL;
		$this->strUserGroup = NULL;
		$this->strUserHash = NULL;
	}

	// debug function

	public function setDebugUserFlag($blnDebugFlag)
	{
		$this->blnDebugUserFlag = $blnDebugFlag;
	}

	public function setDebugProductFlag($blnDebugFlag)
	{
		$this->blnDebugProductFlag = $blnDebugFlag;
	}

	public function setDebugOrderFlag($blnDebugFlag)
	{
		$this->blnDebugOrderFlag = $blnDebugFlag;
	}

	public function setDebugConfigFlag($blnDebugFlag)
	{
		$this->blnDebugConfigFlag = $blnDebugFlag;
	}

	public function setDebugBackendFlag($blnDebugFlag)
	{
		$this->blnDebugBackendFlag = $blnDebugFlag;
	}

	// backend function

	public function setBackendV3Link($strBackendLink)
	{
		$this->strBackendV3Link = $strBackendLink;
	}

	// admin function

	public function setAdminUID($strAdminID)
	{
		$this->strAdminUID = $strAdminID;
	}

	public function setAdminSID($strAdminID)
	{
		$this->strAdminSID = $strAdminID;
	}

	public function setUserSID($strUserID)
	{
		$this->strUserSID = $strUserID;
	}

	// user config function

	public function setUserRole($strRole)
	{
		$this->strUserRole = $strRole;
	}

	public function setUserGroup($strGroup)
	{
		$this->strUserGroup = $strGroup;
	}

	// user function

	public function setUserFirstName($strFirstName)
	{
		$this->strUserFirstName = $strFirstName;
	}

	public function setUserMiddleName($strMiddleName)
	{
		$this->strUserMiddleName = $strMiddleName;
	}

	public function setUserLastName($strLastName)
	{
		$this->strUserLastName = $strLastName;
	}

	public function setUserEmail($strEmail)
	{
		$this->strUserEmail = $strEmail;
	}

	public function getUserHash()
	{
		return $this->strUserHash;
	}

	public function processOrder():bool
	{
		$blnOkFlag = $this->createUser();

		if ($this->blnDebugOrderFlag)
		{
			echo 'debug create user ', var_export($blnOkFlag, true), "\n";
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->updateUserField('firstname', $this->strUserFirstName);

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug update user first name ', var_export($blnOkFlag, true), "\n";
			}
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->updateUserField('middlename', $this->strUserMiddleName);

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug update user middle name ', var_export($blnOkFlag, true), "\n";
			}
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->updateUserField('user_agreement', 'yes');

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug update user agreement ', var_export($blnOkFlag, true), "\n";
			}
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->updateUserField('sub_language', 'language_nl');

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug update user language ', var_export($blnOkFlag, true), "\n";
			}
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->addUserToGroup();

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug add user group ', var_export($blnOkFlag, true), "\n";
			}
		}

		if ($blnOkFlag)
		{
			$blnOkFlag = $this->updateUserField('Status', 'inactive');

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug update user status inactive ', var_export($blnOkFlag, true), "\n";
			}
		}

		return $blnOkFlag;
	}

	// check config valid

	private function checkConfig(bool $blnLoginFlag)
	{
		$blnCheckFlag = true;

		// check backend-link
		if (empty($this->strBackendV3Link))
		{
			$blnCheckFlag = false;

			if ($this->blnDebugConfigFlag)
			{
				echo 'debug check config, no backend link', "\n";
			}
		}

		// check admin
		if (empty($this->strAdminUID) ||
			empty($this->strAdminSID) ||
			empty($this->strUserSID)
		)
		{
			$blnCheckFlag = false;

			if ($this->blnDebugConfigFlag)
			{
				echo 'debug check config, no admin login', "\n";
			}
		}

		return $blnCheckFlag;
	}

	// create new user
	private function createUser(): bool
	{
		$blnOkFlag = $this->checkConfig(false);

		if ($blnOkFlag)
		{
			$strBackendLink = $this->strBackendV3Link;
			$strBackendLink .= '?Action=CreateUser';
			$strBackendLink .= '&Key=' . $this->strAdminSID;
			$strBackendLink .= '&Uid=' . $this->strAdminUID;

			// create user name
			$strUserNumber = random_int(1, 9999);
			if (strlen($strUserNumber) < 4)
			{
				$strUserNumber = str_pad('', 4 - strlen($strUserNumber), '0') . $strUserNumber;
			}
			$this->strUserName = str_replace(' ', '', $this->strUserFirstName . '.');
			if (! empty($this->strUserMiddleName))
			{
				$this->strUserName .= $this->strUserMiddleName . '.';
			}
			$this->strUserName .= $this->strUserLastName;
			$this->strUserName .= $strUserNumber;

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug lastname = ', $this->strUserLastName, "\n";
				echo 'debug username = ', $this->strUserName, "\n";
				echo 'debug email = ', $this->strUserEmail, "\n";
			}

			// build subject-fields
			$strBackendLink .= '&Lastname=' . rawurlencode($this->strUserLastName);
			$strBackendLink .= '&Username=' . rawurlencode($this->strUserName);
			$strBackendLink .= '&Email=' . rawurlencode($this->strUserEmail);
			$strBackendLink .= '&Status=active';
			$strBackendLink .= '&Role=' . $this->strUserRole;

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug create-link = ', $strBackendLink, "\n";
			}

			// backend is REST-interface, V3 GET
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_HTTPGET, 1);
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug create user = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response = ', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Proceed when we're certain that the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				try
				{
					// Turn the response into a new object
					$objCreateResponse = new SimpleXMLElement($varResponse);

					// check if an error occurred
					if (isset($objCreateResponse->Error))
					{
						$blnOkFlag = false;

						if ($this->blnDebugBackendFlag)
						{
							echo 'debug create user error = ', var_export($objCreateResponse->Message, true), "\n";
						}
					}
					else
					{
						// get user hash from response
						$this->strUserHash = (string)$objCreateResponse->Uid;
					}
				}
				catch (Exception $objE)
				{
					$blnOkFlag = false;
				}
			}
			else
			{
				$blnOkFlag = false;
			}
		}

		return $blnOkFlag;
	}

	// Let's make the call: add user to group
	private function addUserToGroup(): bool
	{
		$blnOkFlag = $this->checkConfig(false);

		if ($blnOkFlag)
		{
			$strBackendLink = $this->strBackendV3Link;
			$strBackendLink .= '?Action=AddUsersToGroup';
			$strBackendLink .= '&Key=' . $this->strAdminSID;
			$strBackendLink .= '&Uid=' . $this->strAdminUID;

			$strBackendLink .= '&name=' . $this->strUserGroup;
			$strBackendLink .= '&users=' . $this->strUserHash;

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug add user to group = ', $this->strUserGroup, "\n";
			}

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug backend-link = ', $strBackendLink, "\n";
			}

			// Create another cURL GET call and handle the response
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_HTTPGET, 1);
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug add user to group = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response = ', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Proceed when we're certain that the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				try
				{
					// Turn the response into a new object
					$objCreateResponse = new SimpleXMLElement($varResponse);

					// check if an error occurred
					if (isset($objCreateResponse->Error))
					{
						$blnOkFlag = false;

						if ($this->blnDebugBackendFlag)
						{
							echo 'debug add user to group error = ', var_export($objCreateResponse->Message, true), "\n";
						}
					}
				}
				catch (Exception $objE)
				{
					$blnOkFlag = false;
				}
			}
			else
			{
				$blnOkFlag = false;
			}
		}

		return $blnOkFlag;
	}

	// Update user field
	private function updateUserField(string $strFieldName, string $strFieldValue): bool
	{
		$blnOkFlag = $this->checkConfig(true);

		if ($blnOkFlag)
		{
			$strBackendLink = $this->strBackendV3Link;
			$strBackendLink .= '?Action=SetUserField';
			$strBackendLink .= '&Key=' . $this->strUserSID;
			$strBackendLink .= '&Uid=' . $this->strUserHash;

			$strBackendLink .= '&Var=' . $strFieldName;
			$strBackendLink .= '&Value=' . $strFieldValue;

			if ($this->blnDebugOrderFlag)
			{
				echo 'debug update field, ', $strFieldName, ' = ', $strFieldValue, "\n";
			}

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug backend-link = ', $strBackendLink, "\n";
			}

			// Create another cURL GET call and handle the response
			$hndCall = curl_init();
			curl_setopt($hndCall, CURLOPT_URL, $strBackendLink);
			curl_setopt($hndCall, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($hndCall, CURLOPT_TIMEOUT, 120);
			curl_setopt($hndCall, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($hndCall, CURLOPT_HTTPGET, 1);
			$varResponse = curl_exec($hndCall);
			$intResponseCode = curl_getinfo($hndCall, CURLINFO_RESPONSE_CODE);

			if ($this->blnDebugBackendFlag)
			{
				echo 'debug update user field = ', $intResponseCode, "\n";
				if ($intResponseCode != 200)
				{
					echo 'debug curl error = ', curl_error($hndCall), "\n";
				}
				echo 'debug curl response = ', var_export($varResponse, true), "\n";
			}

			curl_close($hndCall);

			// Proceed when we're certain that the response is successful
			if (($intResponseCode == 200) && ($varResponse !== false))
			{
				try
				{
					// Turn the response into a new object
					$objCreateResponse = new SimpleXMLElement($varResponse);

					// check if an error occurred
					if (isset($objCreateResponse->Error))
					{
						$blnOkFlag = false;

						if ($this->blnDebugBackendFlag)
						{
							echo 'debug update user field error = ', var_export($objCreateResponse->Message, true), "\n";
						}
					}
				}
				catch (Exception $objE)
				{
					$blnOkFlag = false;
				}
			}
			else
			{
				$blnOkFlag = false;
			}
		}

		return $blnOkFlag;
	}
}